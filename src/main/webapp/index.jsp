
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consulta Palabras</title>
    </head>
    <body>
        <h1>Significado Palabras</h1>
        <form method="post" id="formulario" action="    ">
            <input type="hidden" name="tipoSolicitud" id="tipo" value="1"/>
            <table>
                <tr>
                    <td>Palabra : </td>
                    <td><input type="text" name="palabra" required placeholder="Ingrese una palabra"></td>
                </tr>
                <tr>
                    <td>${palabra}</td>    
                    <td><textarea disabled="disabled">${definicion}</textarea></td>
                </tr>
                <tr>
                     <td></td>
                     <td><input type="submit" value="Consulta Palabra" /></td>
                </tr>
            </table>
            <br>
            <input type="button" id="historial" onclick="verHistorial()" value="Ver historial de palabras consultadas" />
        </form>
    <hr> 
    <table>
        <th>
            <td>Palabra</td>
            <td>Fecha</td>
        </th>
        
        
    <c:forEach items="${listaHistorial}" var="historico">
        <tr>
            <td><c:out value="${historico.palabra}"/></td>
            <td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${historico.fechaIngreso}" /></td>
        </tr>
    </c:forEach>

    </table>
    </body>
    <script>
        function verHistorial(){
            document.getElementById("tipo").value = 2;
            document.getElementById("formulario").submit();
        }
    </script> 
    
</html>
