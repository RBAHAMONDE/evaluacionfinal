/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenfinal.resources;

import com.mycompany.examenfinal.resources.services.PalabraServicio;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/dictionary")
public class PalabraDictionary {
    
    @GET
    @Path("/obtenerDefinicionPalabra/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObtenerDefiniciones(@PathParam("palabra") String palabra){
        PalabraServicio palabraServicio = new PalabraServicio();
        String resultado = palabraServicio.getObtenerDeficionPalabra(palabra);
        if(!resultado.isEmpty()){
            return Response.ok().entity(resultado).build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No fue encontrada la definicion de la palabra").build();
    }
}
