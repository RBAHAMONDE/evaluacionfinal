/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenfinal.resources.services;

import com.mycompany.examenfinal.modelo.ResultadoConsultaPalabra;
import java.util.HashMap;
import static javax.swing.UIManager.get;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.AbstractMultivaluedMap;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;


public class PalabraServicio {

    public String getObtenerDeficionPalabra(String palabra) {
        String resultado = "";
        try {
            String url = "https://od-api.oxforddictionaries.com/api/v2/entries/es/{palabra}?fields=definitions&strictMatch=false";
            String urlDefinitiva = url.replace("{palabra}", palabra);
            Client client = ClientBuilder.newClient();
            WebTarget myResource1 = client.target(urlDefinitiva);
            ResultadoConsultaPalabra algo;
            algo = myResource1.request(MediaType.APPLICATION_JSON)
                    .headers(this.obtenerCabeceras())
                    .get(ResultadoConsultaPalabra.class);
            resultado = this.obtenerDefinicionesDeResultado(algo);
            return resultado;
        } catch (Exception e) {
            System.out.println("Sin coincidencias para la palabra, mensaje de error, "+e.getMessage());
        }
        return resultado;

    }

    private MultivaluedMap<String, Object> obtenerCabeceras() {
        MultivaluedMap parametros = new MultivaluedHashMap<String, Object>();
        parametros.add("app_id", 87438914);
        parametros.add("app_key", "1eff1f9ba2c855b5406b59481b719bbb");
        return parametros;
    }

    private String obtenerDefinicionesDeResultado(ResultadoConsultaPalabra rcp) {
        String definicion = null;
        try {
            definicion = rcp.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().get(0);
        } catch (NullPointerException ex) {
            System.out.println("no vienen resultados : " + ex);
        }
        return definicion;
    }
}
