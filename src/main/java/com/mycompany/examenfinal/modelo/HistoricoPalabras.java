/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenfinal.modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "historico_palabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HistoricoPalabras.findAll", query = "SELECT h FROM HistoricoPalabras h"),
    @NamedQuery(name = "HistoricoPalabras.findByCodigoHistorico", query = "SELECT h FROM HistoricoPalabras h WHERE h.codigoHistorico = :codigoHistorico"),
    @NamedQuery(name = "HistoricoPalabras.findByPalabra", query = "SELECT h FROM HistoricoPalabras h WHERE h.palabra = :palabra"),
    @NamedQuery(name = "HistoricoPalabras.findByFechaIngreso", query = "SELECT h FROM HistoricoPalabras h WHERE h.fechaIngreso = :fechaIngreso")})
public class HistoricoPalabras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codigo_historico")
    private Integer codigoHistorico;
    @Size(max = 250)
    @Column(name = "palabra")
    private String palabra;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    public HistoricoPalabras() {
    }

    public HistoricoPalabras(Integer codigoHistorico) {
        this.codigoHistorico = codigoHistorico;
    }

    public Integer getCodigoHistorico() {
        return codigoHistorico;
    }

    public void setCodigoHistorico(Integer codigoHistorico) {
        this.codigoHistorico = codigoHistorico;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoHistorico != null ? codigoHistorico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HistoricoPalabras)) {
            return false;
        }
        HistoricoPalabras other = (HistoricoPalabras) object;
        if ((this.codigoHistorico == null && other.codigoHistorico != null) || (this.codigoHistorico != null && !this.codigoHistorico.equals(other.codigoHistorico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.examenfinal.modelo.HistoricoPalabras[ codigoHistorico=" + codigoHistorico + " ]";
    }
    
}
